<?php ?>

<?php $theme_color = get_field( 'header_theme_color', 'option' ); ?>
  <div class="header <?php echo $theme_color ?>">

    <div class="header__row">
        <div class="header__logo">
        <div class="logo">
          <?php $logo_header = get_field( 'logo_header', 'option' ); ?>
          <?php if ( $logo_header ) { ?>
            <a href="<?php echo esc_url(home_url('/')); ?>">
              <img src="<?php echo $logo_header['url']; ?>" alt="<?php echo $logo_header['alt']; ?>" />
            </a>
          <?php } ?>  
        </div>
        </div>

        <div class="header__menu">
            <input class="menu-btn" type="checkbox" id="menu-btn" />
            <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
            <?php
              wp_nav_menu(
                  array(
                    'theme_location' => 'menu-1',
                    'container' => '',
                    'container_class' => '',
                    'menu_class' => 'menu',
                    'fallback_cb' => '',
                    'menu_id' => 'menu'
                  )
              );
            ?>
        </div>

    </div>
</div>