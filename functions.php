<?php
/**
 * Joe Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Joe_Theme
 */

if ( ! function_exists( 'joe_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function joe_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Joe Theme, use a find and replace
		 * to change 'joe-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'joe-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
      'menu-1' => esc_html__( 'Primary', 'joe-theme' ),
      'menu-2' => esc_html__( 'Secondary', 'joe-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'joe_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'joe_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function joe_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'joe_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'joe_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function joe_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'joe-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'joe-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'joe_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function joe_theme_scripts() {

 
  // load bootstrap js
  wp_enqueue_script('joe-theme-style-bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),'',true);


	wp_enqueue_style( 'joe-theme-style', get_stylesheet_uri() );

	wp_enqueue_script( 'joe-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'joe-theme-slider', get_template_directory_uri() . '/js/swiper.min.js', array(), '20151216', true );
	wp_enqueue_script( 'joe-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'joe_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


require get_template_directory() . '/inc/panel-setting.php';
// require get_template_directory() . '/inc/blocks.php';
require get_template_directory() . '/inc/shortcode.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


function my_custom_posttypes() {

  /* ========================CONNECTORS======================= */
        $labels_modal = array(
          'name'               => 'Modal',
          'singular_name'      => 'Modal',
          'menu_name'          => 'Modal',
          'name_admin_bar'     => 'Modal',
          'add_new'            => 'Add New Modal',
          'add_new_item'       => 'Add New Modal',
          'new_item'           => 'New Modal',
          'edit_item'          => 'Edit Modal',
          'view_item'          => 'View Modal',
          'all_items'          => 'All Modals',
          'search_items'       => 'Search Modals',
          'parent_item_colon'  => 'Parent Modal:',
          'not_found'          => 'No Modal found.',
          'not_found_in_trash' => 'No Modal found in Trash.',
      );
  
      $args_modal = array(
          'labels'             => $labels_modal,
          'public'             => true,
          'publicly_queryable' => true,
          'show_ui'            => true,
          'show_in_menu'       => true,
          'menu_icon'          => 'dashicons-networking',
          'query_var'          => true,
          'rewrite'            => array( 'slug' => 'modals', "with_front" => false ),
          'capability_type'    => 'post',
          'has_archive'        => true,
          'hierarchical'       => false,
          'menu_position'      => 5,
          'supports'           => array('title','editor'),
      );
      /* ========================CONNECTORS======================= */
  
  
  
      register_post_type( 'modals', $args_modal);
  
  }
  add_action( 'init', 'my_custom_posttypes' );
  
  
  // Flush rewrite rules to add "review" as a permalink slug
  function my_rewrite_flush() {
      my_custom_posttypes();
      flush_rewrite_rules();
  }
  register_activation_hook( __FILE__, 'my_rewrite_flush' );


  /**
 * Adding Builder to the Post.
 */
function divi_add_new_custom_post_types( $post_types ) {
  $new_post_types_slugs = array(
  'modals'
  );
  $post_types = array_merge( $post_types, $new_post_types_slugs );
  return $post_types;
  }
  add_filter( 'et_builder_post_types', 'divi_add_new_custom_post_types' );


//   function my_plugin_body_class($classes) {
  
//     $theme_color = get_field( 'header_theme_color', 'option' );
//     $classes[] = $theme_color;
//     return $classes;
// }

// add_filter('body_class', 'my_plugin_body_class');