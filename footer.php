<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Joe_Theme
 */

?>

	</div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 30,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
      }
    });

    jQuery(document).ready(function () {

      jQuery(".view_more_row").hide();
      jQuery("#featured_toggle span").click(function () {

      jQuery(".view_more_row").fadeToggle('slow');
      jQuery("#featured_toggle").hide();


    });
    });


  </script>

</body>
</html>
