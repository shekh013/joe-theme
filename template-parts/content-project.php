<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Joe_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="project-wrapper">
      <div>
      <?php the_post_thumbnail('thumbnail'); ?>
      </div>
      <div>
      <h3><?php the_title(); ?></h3>
      <a href="<?php the_permalink()?>">More Information</a>
      </div>
  </div>


</article>
