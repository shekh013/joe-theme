<?php


/** 1. customize Panel path */
add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path($path) {
    $path = get_stylesheet_directory() . '/inc/panel/';
    return $path;
}

/** 2. customize Panel dir */
add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir($dir) {
    $dir = get_stylesheet_directory_uri() . '/inc/panel/';
    return $dir;
}

/** 3. Hide Panel field group menu item | Hide = '__return_false'  |  Show= '__return_true' */
add_filter('acf/settings/show_admin', '__return_false');
//add_filter('acf/settings/show_admin', '__return_false');

/** 4. Include Panel */
include_once( get_stylesheet_directory() . '/inc/panel/acf.php' );

/**
 * Theme Setting Menu from ACF
 */

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme Setting',
        'menu_title' => 'Theme Setting',
        'menu_slug' => 'theme_settings',
        'capability' => 'edit_posts',
        'icon_url' => 'dashicons-welcome-view-site',
        'redirect' => false,
    ));
}



