<?php

add_shortcode('modal_button', 'shortcode_modal_button');

function shortcode_modal_button($atts, $content = null) {
    extract( shortcode_atts( array(
    'id'	=> '',
    'title' => ''
	), $atts ) );
    ob_start();

  // Loop to Modal
  $args= array( 'post_type' => 'modals', 
              'posts_per_page' => 1 ,
              'p' => $id
             );

  $main_post = new WP_Query($args);
    ?>
<!-- HTML Code Goes Here -->

<?php while ($main_post->have_posts()) : $main_post->the_post(); ?>

<div class="modal-button-wrapper">
  
        <a href="" data-target="#portfolioModal<?php echo $id ?>" data-toggle="modal" class="wp-sk-pop-menu-span"> 
            <?php echo $title;?>
        </a>
</div>

<?php add_action('wp_footer', function() use ( $id ) { 
               sk_append_content( $id); }); ?>

<?php endwhile; ?>
<!-- END of HTML Code --> 


<?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [modal_button title="" link=""][/modal_button] **/



function sk_append_content($id) {
    
  $args= array( 'post_type' => 'modals', 
  'posts_per_page' => 1 ,
  'p' => $id
  );

  $main_post = new WP_Query($args);


  ob_start(); ?>
   <div class="portfolio portfolio-modal modal fade" id="portfolioModal<?php echo $id ?>" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-content">
       <div class="close-modal" data-dismiss="modal">
           <div class="lr">
               <div class="rl">
               </div>
           </div>
       </div>
       
      
       
       <div class="container">
           <div class="row">
               <div class="col-lg-12">
                   <div class="modal-body">

                      <?php while ($main_post->have_posts()) : $main_post->the_post(); ?>
                      <h1><?php the_title();?></h1>
                       <hr class="star-primary">
                      <?php the_content(); ?>
                      <?php endwhile; ?>

                       <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                   </div>
               </div> 
           </div>
       </div>
       
   </div>
</div>

<?php 
$content_data = ob_get_contents();
ob_end_clean();
echo $content_data;
}




add_shortcode('modal_box', 'shortcode_modal_box');

function shortcode_modal_box($atts, $content = null) {
    extract( shortcode_atts( array(
    'id'	=> '',
    'title' => ''
  ), $atts ) );
  

    ob_start();

        // Loop to Modal
        $args= array( 'post_type' => 'modals', 
        'posts_per_page' => 1 ,
        'p' => $id
       );
    
        $main_post = new WP_Query($args);


    ?>


<?php while ($main_post->have_posts()) : $main_post->the_post(); ?>
  <!-- HTML Code Goes Here -->
  <div class="modal-box-wrapper">
    <a href="" data-target="#portfolioModal<?php echo $id ?>" data-toggle="modal" class="wp-sk-pop-menu-span"> 
        <?php echo $title;?>
    </a>
  </div>
  <!-- END of HTML Code --> 
<?php endwhile; ?>
    

<?php add_action('wp_footer', function() use ( $id ) { 
               sk_append_content( $id); }); ?>

<?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [modal_box title="" link=""][/modal_box] **/